const error = (message) => {
  throw new Error(message)
}

const required = (variableName) => process.env[variableName] || error(`environment variable ${variableName} is not set`)

module.exports = {
  couchUrl: process.env.COUCH_URL || 'http://localhost:5984',
  searchHost: 'search-ocelot-6tqsxtl7pweckdfvle6ty47yiu.us-east-1.es.amazonaws.com',
  searchUserAccessKeyId: required('SEARCH_USER_ACCESS_KEY_ID'),
  searchUserSecretAccessKey: required('SEARCH_USER_SECRET_ACCESS_KEY')
}
