const React = require('react')
const HelloWorld = require('../src/HelloWorld')
const {shallow} = require('enzyme')

describe('HelloWorld', () => {
    it('renders', () => {
        const wrapper = shallow(<HelloWorld/>)

        wrapper.should.not.equal(null)
    })
})
