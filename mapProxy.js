const httpProxy = require('http-proxy')
const proxy = httpProxy.createProxyServer({ rejectUnauthorized: false })

proxy.on('error', (error) => console.error(error))

const googleMapsApiKey = process.env.GOOGLE_MAPS_API_KEY

if (!googleMapsApiKey) throw new Error('GOOGLE_MAPS_API_KEY environment variable not found')

module.exports = (req, res) =>
  proxy.web(req, res, { target: `https://maps.googleapis.com/maps/api/js?key=${googleMapsApiKey}` })
