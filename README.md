# Project template for UI and services

Scripts:

+ `build` - builds bundle and CSS to /public
+ `dev` - dev mode; webpack bundles hot reloads in memory, less recompiles on demand, server restarts if server.js or anythink in /services changes.
+ `start` - start in production mode (won't work unless you build first).
+ `test` - run mocha tests
+ `test:dev` - run mocha tests, watching for changes and re-running.
