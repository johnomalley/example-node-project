const title = 'Example Project'

const googleMapsApiKey = '???'

module.exports = `
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>${title}</title>
  <link rel="stylesheet" type="text/css" href="/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="/styles.css">
  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=${googleMapsApiKey}" charset="utf-8"></script>
</head>
<body>
  <div id="main"></div>
  <script type="text/javascript" src="/bundle.js" charset="utf-8"></script>
</body>
</html>
`
