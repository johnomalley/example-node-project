import React from 'react'
import { Link } from 'react-router'

const App = () => (
  <div>
    <h1>Example App</h1>
    <ul role="nav">
      <li><Link to="/about">About</Link></li>
      <li><Link to="/components">Components</Link></li>
      <li><Link to="/auth">Authentication</Link></li>
      <li><Link to="/map">Map</Link></li>
    </ul>
  </div>
)

App.displayName = 'App'

export default App
