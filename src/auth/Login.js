import React, {PropTypes as T} from 'react'
import { ButtonToolbar, Button } from 'react-bootstrap'
import AuthService from './AuthService'

export default class Login extends React.Component {
  static contextTypes = {
    router: T.object
  }

  static propTypes = {
    location: T.object,
    auth: T.instanceOf(AuthService)
  }

  render () {
    const {auth} = this.props

    return (
        <div className='root'>
            <h2>Login</h2>
            <ButtonToolbar className='toolbar'>
                <Button bsStyle="primary" onClick={() => auth.login()}>Login</Button>
            </ButtonToolbar>
        </div>
    )
  }
}
