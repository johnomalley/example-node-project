import React, { PropTypes as T } from 'react'
import Button from 'react-bootstrap/lib/Button'
import Alert from 'react-bootstrap/lib/Alert'
import ExampleForm from './ExampleForm'
import ExamplePanel from './ExamplePanel'

const buttons = [
  { style: 'primary', text: 'Next', icon: 'arrow-right' },
  { style: 'danger', text: 'Delete', icon: 'times' },
  { style: 'success', text: 'New', icon: 'plus' },
  { style: 'warning', text: 'Warning', icon: 'exclamation-circle' },
  { style: 'info', text: 'Information', icon: 'question-circle-o' },
  { style: 'default', text: 'Save', icon: 'save' }
]

const alerts = [
  {
    style: 'danger',
    title: 'Error',
    message: 'An error occurred'
  },
  {
    style: 'warning',
    title: 'Warning',
    message: 'This is a warning'
  },
  {
    style: 'success',
    title: 'Good Job',
    message: 'You were successful'
  },
  {
    style: 'info',
    title: 'Information',
    message: 'This is an informational message'
  }
]

const Example = ({ children }) =>
  <div className='example'>
    {children}
  </div>

Example.displayName = 'Example'

Example.propTypes = {
  children: T.node
}

const AlertExample = ({ style, title, message }) =>
  <Example key={`alert_${style}`}>
    <Alert bsStyle={style}>
      <strong>{title}</strong>
      {` ${message}`}
    </Alert>
  </Example>

AlertExample.displayName = 'AlertExample'

AlertExample.propTypes = {
  style: T.string,
  title: T.string,
  message: T.string
}

const Components = () => {
  //noinspection Eslint
  const renderButton = ({style, text, icon}) =>
    <Example key={`button_${style}`}>
      <Button bsStyle={style}>
        {text}
        {' '}
        <i className={`fa fa-${icon}`}/>
      </Button>
    </Example>

  //noinspection Eslint
  const renderAlert = ({ style, title, message }) =>
    <Example key={`alert_${style}`}>
      <Alert bsStyle={style}>
        <strong>{title}</strong>
        {` ${message}`}
      </Alert>
    </Example>

  const examples = buttons.map(renderButton).concat(alerts.map(renderAlert))

  return (
    <div className='examples'>
      {examples}
      <Example>
        <ExampleForm/>
      </Example>
      <Example>
        <ExamplePanel/>
      </Example>
    </div>
  )
}

Components.displayName = 'Components'

export default Components
