import React from 'react'

const HelloWorld = () => (
  <h1>Hello, world</h1>
)

HelloWorld.displayName = 'HelloWorld'

export default HelloWorld
