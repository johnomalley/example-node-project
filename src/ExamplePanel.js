import React from 'react'
import Panel from 'react-bootstrap/lib/Panel'

const ExamplePanel = () => {
  const title = <h3>Panel title</h3>
  return (
    <div>
      <Panel header="Panel heading without title">
        Panel content
      </Panel>
      <Panel header={title}>
        Panel content
      </Panel>
    </div>
  )
}

ExamplePanel.displayName = 'ExampleForm'

export default ExamplePanel
