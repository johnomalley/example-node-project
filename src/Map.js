import React from 'react'

const mapStyle = {
  margin: 20,
  width: 800,
  height: 500,
  border: '1px solid black'
}

const center = {
  lat: 38.632351,
  lng: -90.228033
}

export default class Map extends React.Component {
  constructor (props, context) {
    super(props, context)
    this.state = {}
  }

  componentDidMount() {
    this.map = new google.maps.Map(this.refs.map, { center, zoom: 16 })
  }

  render() {
    return (
      <div ref="map" style={mapStyle}/>
    )
  }
}
