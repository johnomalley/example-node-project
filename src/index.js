import React from 'react'
import { render } from 'react-dom'
import { Router, Route, browserHistory } from 'react-router'
import App from './App'
import About from './About'
import Components from './Components'
import Authentication from './Authentication'
import AuthService from './auth/AuthService'
import Login from './auth/Login'
import Map from './Map'

const auth = new AuthService('AkO3gnKJqhHFE6Be6xiWfINFdYbF95qH', 'larry.auth0.com')

const LoginWrapper = () => <Login auth={auth}/>
const AuthenticationWrapper = () => <Authentication auth={auth}/>

const requireAuth = (nextState, replace) => {
  auth.parseAuthHash(nextState.location.hash)
  if (!auth.loggedIn()) {
    replace({ pathname: '/login' })
  }
}

const routes =
  <Router history={browserHistory}>
    <Route path="/" component={App}/>
    <Route path="/about" component={About}/>
    <Route path="/components" component={Components}/>
    <Route path="/auth" component={AuthenticationWrapper} onEnter={requireAuth}/>
    <Route path="/login" component={LoginWrapper}/>
    <Route path="/map" component={Map}/>
  </Router>

render(routes, document.getElementById('main'))
